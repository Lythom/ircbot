exports.init = init;
exports.unload = unload;
exports.help = help;

var welcomeMessages = [
  'salut',
  'bonjour',
  'slt',
  'plop',
  'yo',
  'yop',
  'bjr',
  'hey',
  'heya',
  'hello',
  'coucou',
  'aloha',
  'wesh'
];

var bot, config;

function init(_bot, _config) {
  bot = _bot;
  config = _config;

  // Listen for any message
  // Answer on the same canal (private or channel).
  bot.registerMessageEvent(answerMessage, 100);
  //bot.addListener("join", sayHelloOnJoin);
}

function unload() {
  bot.removeMessageEvent(answerMessage);
  //bot.removeListener("join", sayHelloOnJoin);
}

function sayHelloOnJoin(channel, nick, message) {
  if (nick.toLowerCase().indexOf(config.botName.toLowerCase()) > -1) {
    bot.sayAndRegister(config.channels[0], 'Bonjour à tous !');
  }
}

function answerMessage(from, to, text, message) {

  var answerTo = to;
  if (to === config.botName) {
    answerTo = from;
  }

  // répond à un bonjour
  for (msgIdx in welcomeMessages) {
    if (!welcomeMessages.hasOwnProperty(msgIdx)) continue;
    var msg = welcomeMessages[msgIdx];

    // répond si le message est général (tolère la ponctuation) ou si le message s'adresse au bot ou à tous en particulier.
    if (text.toLowerCase().indexOf(msg) > -1) {
      if (text.toLowerCase().indexOf(config.botName.toLowerCase()) > -1 || text.toLowerCase().indexOf('tous') > -1 || text.toLowerCase().indexOf('all') > -1 || text.toLowerCase().indexOf('monde') > -1) {
        if (config.verbose || text.toLowerCase().indexOf(config.botName.toLowerCase()) > -1) {
          bot.sayAndRegister(answerTo, 'Bonjour à toi ' + from + ' !');
        }
        return true;
      } else if (text.length <= msg.length + 5) {
        if (config.verbose) {
          bot.sayAndRegister(answerTo, 'Bonjour ' + from + ' !');
        }
        return true;
      }
    }
  }
  if (text.toLowerCase().indexOf('o/') === 0) {
    if (config.verbose) {
      bot.sayAndRegister(answerTo, '\\o');
    }
    return true;
  } else if (text.toLowerCase().indexOf('\\o') === 0) {
    if (config.verbose) {
      bot.sayAndRegister(answerTo, 'o/');
    }
    return true;
  }

  return false;
}

function help() {
  return "Welcome :\n" +
    "  Salue les personnes qui disent bonjour ou qui saluent spécifiquement le bot ou l'ensemble des personnes présentes.";
}
