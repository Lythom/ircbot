exports.init = init;
exports.unload = unload;
exports.help = help;

var bot,config;

function init(_bot, _config) {
    bot = _bot;
    config = _config;

    // Listen for any message
    // Answer on the same canal (private or channel).
    bot.registerMessageEvent(answerMessage, 100);
}

function unload() {
    bot.removeMessageEvent(answerMessage);
}

function sayHelloOnJoin (channel, nick, message) {
    if (nick.toLowerCase().indexOf(config.botName.toLowerCase()) > -1) {
        bot.say(config.channels[0], 'Bonjour à tous !');
    }
}

function answerMessage (from, to, text, message) {

    var answerTo = to;
    if (to === config.botName) {
        answerTo = from;
    }

    // !help or <botname> help or man <botname>.
    if (text === '!help' || text === '!aide'
        || (text.toLowerCase().indexOf(config.botName.toLowerCase()) > -1 
            && text.toLowerCase().indexOf('help') > -1)
        || (text.toLowerCase().indexOf(config.botName.toLowerCase()) > -1 
            && text.toLowerCase().indexOf('man') > -1)
        || (text.toLowerCase().indexOf(config.botName.toLowerCase()) > -1 
            && text.toLowerCase().indexOf('aide') > -1)
    ) {
        var helpTxt = 'Available Modules \n';
        helpTxt += '\n-------------\n';
        for(iModule in bot.modulesLoaded) {
            if (!bot.modulesLoaded.hasOwnProperty(iModule)) continue;
            var module = bot.modulesLoaded[iModule];
            helpTxt += '> ' + module.help() + '\n';
        }
        if(answerTo != from){
            bot.say(answerTo, "Réponse en PM pour éviter le flood ici.");
        }
        bot.say(from, helpTxt);
        return true;
    }

    if (text === '!join #garden' && from === 'Samuel'){
        bot.say('Samuel', "joining #garden");
        bot.join('#garden');
    }

    if (text === '!part #garden' && from === 'Samuel'){
        bot.say('Samuel', "parting #garden");
        bot.part('#garden');
    }

    return false;

}

function help(){
    return "Help :\n"+
           "  !help / !aide / help/aide/man <botname> : Affiche ce message d'aide";
}