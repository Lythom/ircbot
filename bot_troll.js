exports.init = init;
exports.unload = unload;
exports.help = help;

var bot, config;

var request = require('request');
var cheerio = require("cheerio");
var randomphrase = require("./randomphrase");


function init(_bot, _config) {
    bot = _bot;
    config = _config;

    // Listen for any message
    // Answer on the same canal (private or channel).
    bot.registerMessageEvent(answerMessage, 10);
}

function unload() {
    bot.removeMessageEvent(answerMessage);
}

function answerMessage(from, to, text, message) {
    try {

        var answerTo = to;
        if (to === config.botName) {
            answerTo = from;
        }
        // compagnie créole
        if (new RegExp('[d|t][e|é][k|c]a[d|t|l][e|é][k|c]a[d|t]a[m|n]', 'gi').test(text)) {
            bot.sayAndRegister(answerTo, '♪ Oh eh Oh eh ♪');
            return true;
        }
        if (new RegExp('au bal', 'gi').test(text)) {
            bot.sayAndRegister(answerTo, '♪ Au bal masqué Oh eh Oh eh ♪');
            return true;
        }

        if (text.toLowerCase().indexOf('elle ne peut pas') === 0) {
            bot.sayAndRegister(answerTo, '♪ S\'arrêter Oh eh Oh eh ♪');
            return true;
        }

        if (new RegExp('!aurevoir mdugue', 'gi').test(text)) {
            bot.sayAndRegister(answerTo, 'mdugue je sais que tu pars ce soir, alors je souhaitais évoquer quelques souvenirs que l\'on a partagé. Voici un extrait de nos échanges :');
            bot.sayAndRegister(answerTo, 'mdugue : TA GUEULE gardenBot');
            bot.sayAndRegister(answerTo, 'mdugue : quel lèche-cul ce gardenBot');
            bot.sayAndRegister(answerTo, 'mdugue : gardenBot: va jouer dans le mixer');
            bot.sayAndRegister(answerTo, 'Merci pour ces échanges mdugue et à bientôt !');
            return true;
        }

        if (config.verbose) {
            if (text.indexOf('<3') === 0) {
                bot.sayAndRegister(answerTo, 'Ԑ>');
                return true;
            }
            if (text.indexOf('Ԑ>') === 0) {
                bot.sayAndRegister(answerTo, '<3');
                return true;
            }
        }
        /*
        if (text.toLowerCase().indexOf('grumpf') > -1
                || text.toLowerCase().indexOf('merde') > -1
                || text.toLowerCase().indexOf('putain') > -1
                || text.toLowerCase().indexOf('put1') > -1
                || text.toLowerCase().indexOf(' chier') > -1
                || text.toLowerCase().indexOf('connard') > -1
                || text.toLowerCase().indexOf('enculé') > -1
                || text.toLowerCase().indexOf('encule') > -1
                || text.toLowerCase().indexOf('enfoiré') > -1
                || text.toLowerCase().indexOf('apple') > -1) {
            var generated = randomphrase.generate(from);
            if (generated) {
                bot.sayAndRegister(answerTo, "Pas d'insultes ici ! Pour la peine je vais révéler une vérité...");
                bot.sayAndRegister(answerTo, generated);
                return true;
            }
            return true;
        }
        */

        // nuit gardenBot
        if (new RegExp('nui+t', 'gi').test(text)) {
            var a = '';
            for (i = 0; i < text.length; i++) {
                if (text.charAt(i) === 'i') {
                    a += 'a';
                }
            }
            bot.sayAndRegister(answerTo, 'L' + a + ' ♪');
            return true;
        }

        if (text.toLowerCase().indexOf('hakuna') > -1) {
            bot.sayAndRegister(answerTo, 'Ce mot signifieeee, Que tu vivras ta viiiiie ♪ Sans aucun soucis ♪ Philosophie ♪');
            return true;
        }

        // coucouche
        if (text.toLowerCase().indexOf('coucouche') > -1 && text.toLowerCase().indexOf('panier') === -1) {
            bot.sayAndRegister(answerTo, "panier");
            return true;
        }

        if (text.toLowerCase().indexOf('!marieprefere') > -1) {
            bot.sayAndRegister(answerTo, "« Je préfère encore la Macsf » - Marie Guillaumet, le 14 Octobre 2014");
            return true;
        }

        if (text.toLowerCase().indexOf('!gabrielprefere') > -1) {
            bot.sayAndRegister(answerTo, "« C'est cool Horyou » - Gabriel, le Jeudi 12 Février 2015");
            return true;
        }


        // troll
        if (new RegExp('!troll (.+)', 'gi').test(text)) {
            var sujet = text.substring(7, text.length);
            var generated = randomphrase.generate(sujet, 60);
            if (generated) {
                bot.sayAndRegister(answerTo, 'Je vais révéler une vérité : ' + generated);
                return true;
            }
            return false;
        }

        // devinette
        if (text.toLowerCase().indexOf('!devinette') > -1 ||
            (text.toLowerCase().indexOf('devinette') > -1 && text.toLowerCase().indexOf(config.botName.toLowerCase()) > -1)) {
            request({
                uri: 'http://www.unedevinette.com/?page=3&anti='+Math.round(Math.random()*999999999999)
            }, function(error, response, body) {
                if (error && response.statusCode !== 200) {
                    console.log('Error when contacting unedevinette.com')
                    return false;
                }
                // parse html
                try {
                    var $ = cheerio.load(body, {
                        normalizeWhitespace: true,
                        xmlMode: false,
                        decodeEntities: false
                    });
                    var devinette = $('#texte').html().replace(/\<br\>/gi,' ').replace('Ce site est une extension de réseau ewakoo','');
                    var reponse =  $('#reponse').html().replace(/\<br\>/gi,' ');
                    bot.sayAndRegister(answerTo, devinette);
                    setTimeout(function() {
                        bot.sayAndRegister(answerTo, reponse);
                    }, 25000)

                } catch (e) {
                    dumpError(e);
                }
            });
        }

        // envoie une des citations en ligne ou pré enregistrée.
        if (text.toLowerCase().indexOf('!quote') > -1 || text.toLowerCase().indexOf('!citation') > -1 ||
            (text.toLowerCase().indexOf('quote') > -1 || text.toLowerCase().indexOf('citation') > -1) && text.toLowerCase().indexOf(config.botName.toLowerCase()) > -1) {

                request({
                    uri: 'http://lapunaise.fr/aleatoires'
                }, function(error, response, body) {
                    if (error && response.statusCode !== 200) {
                        console.log('Error when contacting lapunaise.fr')
                        return false;
                    }
                    // parse html
                    try {
                        var $ = cheerio.load(body);
                        var quoteOK = false;
                        var quoteId = 0;
                        var quoteNode = null;
                        do {
                            quoteNode = $('.proverbe a')[quoteId];
                            quoteId++;
                            if(quoteNode.attribs.href.indexOf('femme') === -1 
                                && quoteNode.attribs.href.indexOf('homme') === -1 
                                && quoteNode.attribs.href.indexOf('sexe') === -1 ) {
                                quoteOK = true;
                            }
                        } while(!quoteOK && quoteId<7);

                        if (quoteOK) {
                            var quote = quoteNode.attribs.title;
                            bot.sayAndRegister(answerTo, quote.replace('\n', '').replace('\r', ''));
                        } else {
                            bot.sayAndRegister(answerTo, '* Blague censurée *');
                        }
                        
                    } catch (e) {
                        dumpError(e);
                    }
                });

            return true;
        }

        if (text.toLowerCase().indexOf('!baseline') > -1 ||
            (text.toLowerCase().indexOf('baseline') > -1 && text.toLowerCase().indexOf(config.botName.toLowerCase()) > -1)
        ) {
            var a = new Array("Premium", "Behavioural", "Synchronized", "Independent", "Upgraded", "Lightweight", "Flexible", "Highly Compressed", "World's Fastest", "Complex", "Powerful", "Scalable", "Fault Tolerant", "User-Centric", "Real", "Universal", "Simple", "Virtual", "B2B", "B2C", "Real time", "Visual", "Fast", "Cross", "Best", "Smooth", "Digital", "Instant", "Hand-crafted", "Global", "Outsourced", "Online", "User-Friendly", "Wireless", "One-To-One", "World First", "Unique", "Global", "Optimized", "Maximised", "Powered");
            var b = new Array("Multi-Channel", "Customer Intelligence", "Neuroeconomic", "In-Memory Database", "Open Data", "Message-Based", "Management", "SoLoMo", "Machines", "Storage", "MoLoSo", "Multi-Platform", "Cloud Computing", "MoSoLo", "Cloud", "Big-Data", "Solar", "Social", "Mobile", "Local", "Data Mining", "Disrupt", "Leads", "Tracking", "CRM", "Business Intelligence", "Cross-Canal", "Customer", "Experience", "Facility Management", "Community", "Viral", "Influence", "Centralized", "ROI", "Inbound Marketing", "Database", "Data Collect", "Universal", "Scale-Out", "Marketplace", "Pocket Size", "Mobile Device", "Wearable", "System", "Molecular Sensor", "Nano-Tech", "Business Facing", "Mobile", "Search", "Publishing");
            var c = new Array("Native", "Reinvigorated", "Based", "Integrated", "Front Faced", "Disruptive", "Organic", "Tracking", "Cloud Based", "Aggretated", "Focused", "Cloud Computed", "Customisable", "Quantum", "Monitored", "Streamed", "Connected", "Scaled", "Leveraged", "Decentralized", "Energized", "Analytics");
            var d = new Array("Business", "Device", "App", "Company", "Action Plans", "Group", "Marketing", "Startup", "Service", "Technology", "Solution", "Software", "Hardware", "System", "Platform", "Methodology", "Models", "Synergy");

            var r1 = a[Math.floor(Math.random() * a.length)];
            var r2 = b[Math.floor(Math.random() * b.length)];
            var r3 = c[Math.floor(Math.random() * c.length)];
            var r4 = d[Math.floor(Math.random() * d.length)];
            var baseline = r1 + " " + r2 + " " + r3 + " " + r4;
            bot.sayAndRegister(answerTo, baseline);
            return true;
        }

        if (text.toLowerCase().indexOf('!kill') > -1 ||
            (text.toLowerCase().indexOf('kill') > -1 && text.toLowerCase().indexOf(config.botName.toLowerCase()) > -1) ||
            (text.toLowerCase().indexOf('ta gueule') > -1 && text.toLowerCase().indexOf(config.botName.toLowerCase()) > -1) ||
            (text.toLowerCase().indexOf('stfu') > -1 && text.toLowerCase().indexOf(config.botName.toLowerCase()) > -1)
        ) {
            bot.part('#garden', 'très bien, à plus tard !');
            return true;
        }

    } catch (e) {
        dumpError(e);
    }

    return false;
}

function help() {
    return "Fun :\n" +
        "  !quote : affiche une citation\n" +
        "  !devinette : pose une devinette\n" +
        "  !baseline : génère une baseline aléatoire\n" +
        "  !kill : demande au bot de s'en aller\n" +
        "  Répond à certaines références =)";
}