exports.init = init;
exports.unload = unload;
exports.help = help;

var fs = require('fs');

var bot,config;

function init(_bot, _config) {
    bot = _bot;
    config = _config;

    // Listen for any message
    // Answer on the same canal (private or channel).
    bot.registerMessageEvent(answerMessage,99999);
}

function unload() {
    bot.removeMessageEvent(answerMessage);
}

function answerMessage (from, to, text, message) {

    // répond à un ping personnel
    if(text.toLowerCase().indexOf(config.botName.toLowerCase()) > -1) {
         fs.appendFile('idees.todo'," ☐ " + from + " : " + text + '\n');
    }
    if (text.toLowerCase().indexOf(config.botName.toLowerCase()) > -1 && text.toLowerCase().indexOf(config.botName.toLowerCase()) < 8) {
        var answerTo = to;
        if (to === config.botName) {
            answerTo = from;
        }
        if (config.verbose) {
            bot.sayAndRegister(answerTo, 'Je ne sais pas répondre à ça ' + from + ', mais je note de côté pour que Samuel puisse implémenter ça plus tard !');
        }
        return true;
    }

    return false;
}

function help(){
    return "Memo :\n"+
           "  En registre toute tentative d'interaction infructueuse avec le bot en vue d'une implémentation ultérieur.";
}