exports.init = init;
exports.unload = unload;
exports.help = help;

var Schema = require('jugglingdb').Schema;
var db, Post;

var bot,config;

function init(_bot, _config) {
    bot = _bot;
    config = _config;

    try {
        db = new Schema('sqlite3', {database: 'history.sqlite'});
        db.on('connected',function(){
            console.log('connected to database');
        });

        db.on('disconnected',function(){
            console.log('disconnected from database');
        })

        // define models
        db.adapter.command("CREATE TABLE IF NOT EXISTS message (id INTEGER PRIMARY KEY, channel TEXT NOT NULL, text TEXT NOT NULL, author TEXT NOT NULL, date TEXT NOT NULL, timestamp INTEGER NOT NULL);",function(result){});
        Post = db.define('message', {
            channel:    { type: String, length: 255 },
            text:       { type: Schema.Text },
            author:     { type: String, length: 255 },
            date:       { type: Date,    default: function () { return new Date;} },
            timestamp:  { type: Number,  default: Date.now }
        });
    } catch(e) {
        dumpError(e);
    }

     // Listen for any message
    // Answer on the same canal (private or channel).
    bot.addListener("message", recordMessage);
    bot.registerMessageEvent(answerMessage,100);

}

function unload() {
    bot.removeListener("message", recordMessage);
    bot.removeMessageEvent(answerMessage);
    db.disconnect();
    db = null;
}

function answerMessage (from, to, text, message) {
    if (text.toLowerCase().indexOf('!history') === 0) {
        try{
            Post.all({ limit: 10}, function(err, result){
                try{
                    var ser = '';
                    for(rIdx in result){
                        if (!result.hasOwnProperty(rIdx)) continue;
                        var r = result[rIdx];
                        ser += r.author + ' said to ' + r.channel + ' : ' + r.text + '.';
                    }
                    if(result){
                        bot.say(from, ser);
                    }
                }catch(e){
                    dumpError(e);
                }
            });
        }catch(e){
            dumpError(e);
        }
    }
}

function recordMessage (from, to, text, message) {
    try {
        var test = new Post({
            channel: to,
            text: text,
            author: from
        });
        if(test){
            test.save(function(err){
                if (err !== null) {
                    console.log('msgsaved : '+err);
                }
            });
        }
    } catch(e) {
        dumpError(e);
    }
}

function help(){
    return "History (wip) :\n"+
           "  !history / !log / !historique [filters...]\n"+
           "    filters : name=<nickname>, startdate=<nickname>, endate=<nickname>\n"+
           "    dates au format jj/mm/aaaa";
}