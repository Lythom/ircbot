exports.init = init;
exports.unload = unload;
exports.help = help;

var fs = require('fs');
var CronJob = require('cron').CronJob;
var jobs = [];

var bot, config;

function init(_bot, _config) {
  bot = _bot;
  config = _config;

  if (jobs.length > 0) {
    unload();
  }

  // midi miam
  jobs.push(new CronJob('00 12 12 * * 1-5', function () {
      if (config.verbose) {
        say = 'J\'ai toujours faim à midi douze !';
        bot.say(config.channels[0], say);
      }
    }, function () {},
    true /* Start the job right now */
  ));


  // soir
  jobs.push(new CronJob('00 50 17 * * 1-5', function () {
      if (config.verbose) {
        bot.say(config.channels[0], 'Bonne soirée à tous !');
        bot.part(config.channels[0]);
      }
    }, function () {},
    true /* Start the job right now */
  ));

  // vendredi
  jobs.push(new CronJob('00 01 10 * * 5', function () {
      if (config.verbose) {
        bot.say(config.channels[0], 'Le vendredi, c\'est permis !');
      }
    }, function () {},
    true /* Start the job right now */
  ));
  jobs.push(new CronJob('00 01 15 * * 5', function () {
      if (config.verbose) {
        bot.say(config.channels[0], 'Le vendredi, c\'est permis !');
      }
    }, function () {},
    true /* Start the job right now */
  ));

  jobs.push(new CronJob('00 16 17 * * 5', function () {
      if (config.verbose) {
        bot.say(config.channels[0], 'Je file, il ne faut pas que je rate mon train :S Bon week end à tous !');
        bot.part(config.channels[0]);
      }
    }, function () {},
    true /* Start the job right now */
  ));

  jobs.push(new CronJob('00 18 17 * * 5', function () {
      if (config.verbose) {
        bot.join("#garden", function () {
          bot.say("#garden", 'Aller je plaisante, envoyez vos demande de !quote =)');
        });
      }
    }, function () {},
    true /* Start the job right now */
  ));

  // lundi matin
  jobs.push(new CronJob('00 00 10 * * 1', function () {
      if (config.verbose) {
        bot.say(config.channels[0], 'Et encore un lundi, bon courage pour cette nouvelle semaine !');
      }
    }, function () {},
    true /* Start the job right now */
  ));

  /*
      var i = 0;
      jobs.push(new CronJob('* * * * * *', function(){
          bot.say("Samuel", 'Test '+ (i++));
        }, function () {},
        true
      ));
  */

}

function unload() {
  for (jobIdx in jobs) {
    if (!jobs.hasOwnProperty(jobIdx)) continue;
    var job = jobs[jobIdx];
    try {
      job.stop();
    } catch (e) {
      dumpError(e);
    }
  }

  jobs = [];
}

function help() {
  return "Events :\n" +
    "  Annonce les différents moments de la journée.";
}
