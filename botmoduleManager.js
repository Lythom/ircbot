exports.init = init;
exports.unload = unload;
exports.help = help;

var fs = require('fs');
var path = require('path');
var _ = require('underscore');

var bot;
var config;

function init(_bot, _config) {

    bot = _bot;
    config = _config;

    loadModules();
    startModules();

    bot.registerMessageEvent(listenReload);
}

function unload() {
    bot.removeMessageEvent(listenReload);
}

function listenReload(from, to, text, message){
    if (to.toLowerCase().indexOf(config.botName.toLowerCase()) > -1 && text === '!reload') {
        bot.say(from, 'Rechargement en cours...');
        reload(from);
        bot.say(from, 'Rechargement terminé : '+_.keys(bot.modulesLoaded).join(', '));

        return true;
    }

    if (to.toLowerCase().indexOf(config.botName.toLowerCase()) > -1 && text === '!list') {
        bot.say(from, 'Mods chargés : '+_.keys(bot.modulesLoaded).join(', '));

        return true;
    }

    return false;
}

function listModules() {
    var myfiles = [];
    var files = fs.readdirSync('./');

    files.forEach( function (file) {
        if(/bot_(.*?)\.js/.test(file)){
            myfiles.push(file);
        }
    });

    return myfiles;
}

function reload(from){
    unloadModules(from);
    loadModules(from);
    startModules();
}

function unloadModules(from) {
    // unload all
    for(var iModule = 0, len=bot.modulesLoaded.length; iModule < len; iModule++) {
        var module = bot.modulesLoaded[iModule];
        try{
            module.unload();
        } catch(e) {
            bot.say(from, 'Impossible de décharger '+ iModule);
            dumpError(e);
        }
    }
    bot.modulesLoaded = [];
}

function loadModules(from){
    console.log('Loading modules...');
    var modules = listModules();
    for(var iModule = 0, len=modules.length; iModule < len; iModule++) {
        var moduleFile = modules[iModule];
        try{
            var filename = __dirname + path.sep + moduleFile;
            delete require.cache[filename];
            console.log('Loading : '+filename);
            bot.modulesLoaded.push(require(filename));
         } catch(e) {
            console.log('Impossible charger '+ filename);
            dumpError(e);
        }
    }
    console.log('All modules loaded.');
}

function startModules(){
    for(var iModule = 0, len=bot.modulesLoaded.length; iModule < len; iModule++) {
        try{
            var module = bot.modulesLoaded[iModule];
            console.log('Starting : '+ iModule);
            module.init(bot,config);
        } catch(e) {
            console.log('Impossible d\'intialiser '+ iModule);
            dumpError(e);
        }
    }
}

function help(){
    return "Mod (re)loader :\n"+
           //"  !reload : Reload all modules (pm only)\n"+
           "  (Admin only !)";
}