exports.init = init;
exports.unload = unload;
exports.help = help;

var moment = require('moment');

var bot,config;

function init(_bot, _config) {
    bot = _bot;
    config = _config;

    // Listen for any message
    // Answer on the same canal (private or channel).
    bot.registerMessageEvent(answerMessage, 10);
}

function unload() {
    bot.removeMessageEvent(answerMessage);
}

function answerMessage (from, to, text, message) {
    try{

        var answerTo = to;
        if (to === config.botName) {
            answerTo = from;
        }

        if (text.toLowerCase().indexOf('!mt') === 0) {
            var startDate = moment()
            var endDate = moment('2015-09-24 00:00:00', 'YYYY-M-DD HH:mm:ss')
            var secondsDiff = endDate.diff(startDate, 'seconds')
            var daysDiff = Math.floor(secondsDiff / (24 * 60 * 60));

            bot.sayAndRegister(answerTo, 'Nombre de jours avant le MT : ' + daysDiff +'.');
            return true;
        }

    } catch(e) {
        dumpError(e);
    }

    return false;
}

function help(){
    return "MT module :\n"+
           "  !mt : temps restant avant le prochain mt";
}