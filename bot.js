var config = {
    channels: ["#garden"],
    server: "80.15.218.95",
    botName: "gardenBot",
    verbose: false
};

// Get the lib
var irc = require("irc");

// Create the bot name
var bot = {};
try {
    bot = new irc.Client(config.server, config.botName, {
        channels: config.channels
    });
} catch (e) {
    dumpError(e);
}


// record what the bot says
var messages = [];

var messageCallbacks = [];

/**
 * global accessible vars
 */
// Quand un module a besoin d'un accès, il enregistre un call back qui est appelé en suivant un ordre de priorité.
// Chaque module doit avoir un ordre de priorité, les petits devant les grands derrière (lower first).
// Dès qu'un module a répondu à une demande, les autres demandes sont ignorées.
// le callback doit avoir la signature suivante : function(from, to, text, message) et retourner true si répondu ou false si ignoré
bot.registerMessageEvent = function(callback, priority) {
    if (messageCallbacks[priority] === undefined) {
        messageCallbacks[priority] = [];
    }
    messageCallbacks[priority].push(callback);
};
// supprime un callback enregistré pour l'évènement "message"
bot.removeMessageEvent = function(callback) {
    for (priority in messageCallbacks) {
        if (!messageCallbacks.hasOwnProperty(priority)) continue;
        var priorityCallbacks = messageCallbacks[priority];
        var callbackIdx = priorityCallbacks.indexOf(callback);
        if (callbackIdx > -1) {
            priorityCallbacks.splice(callbackIdx, 1);
        }
    }
};
// liste des modules chargés (lecture seule)
bot.modulesLoaded = [];

bot.sayAndRegister = function(to, text) {
    bot.say(to, text);
    messages.push({
        channel: to,
        text: text,
        author: config.botName
    });
};

function answerMessage(from, to, text, message) {

    console.log(from + '->' + to + ' : ' + text + '/' + message);
    for (priority in messageCallbacks) {
        if (!messageCallbacks.hasOwnProperty(priority)) continue;
        var priorityCallbacks = messageCallbacks[priority];
        for (callbackIdx in priorityCallbacks) {
            if (!priorityCallbacks.hasOwnProperty(callbackIdx)) continue;
            var cb = priorityCallbacks[callbackIdx];
            try {
                var ret = cb(from, to, text, message);
                if (ret === true) {
                    return;
                }
            } catch (e) {
                dumpError(e);
            }
        }
    }
}

// charge et initialise les modules
var moduleLoader = require("./botmoduleManager");
moduleLoader.init(bot, config);

function dumpError(e) {
    if (typeof e === 'object') {
        if (e.message) {
            console.log('\nMessage: ' + e.message)
        }
        if (e.stack) {
            console.log('\nStacktrace:')
            console.log('====================')
            console.log(e.stack);
        }
    } else {
        console.log('dumpError :: argument is not an object');
    }
}

// error management
bot.addListener('error', function(message) {
    console.log('error : ', message);
});
bot.addListener('registered', function(message) {
    console.log('registered : ', message);
});
bot.addListener('notice', function(from, to, text, message) {
    console.log('Notice : ' + from + '->' + to + ' : ' + text + '/' + message);
});

// initialise la logique de callback de messages
bot.addListener("message", answerMessage);

console.log('Trying to connect to server...');